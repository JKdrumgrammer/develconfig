cd $HOME
echo "Work on " `pwd`

# oh-my-zsh
rm -rf $HOME/.oh-my-zsh
export ZSH="$HOME/.oh-my-zsh"; sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

# zsh-syntax-highlighting
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
git clone git://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions

isWSL=`uname -a | grep -i -e microsoft`
if [[ "$isWSL" != "" ]]; then
    mkdir -p ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/lib
    cp -fp $HOME/develconfig/misc.zsh.custom ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/lib/misc.zsh
    chmod +x ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/lib/misc.zsh
fi

# golang
if [ "`command -v go`" == "" ]; then
yum install -y golang
fi 

# fzf
rm -rf $HOME/.fzf
rm -f $HOME/.fzf.zsh
rm -f $HOME/.fzf.bash
#git clone --depth 1 https://github.com/junegunn/fzf.git $HOME/.fzf
#$HOME/.fzf/install

# fzf-vim
curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
vim -c :PlugInstall -c :GoInstallBinaries -c :qa

ln -sfv ~/develconfig/zshrc ~/.oh-my-zsh/custom/ohmyzshrc.zsh 
mkdir -p ~/.oh-my-zsh/custom/themes
ln -sfv ~/develconfig/newro_vcs.zsh-theme ~/.oh-my-zsh/custom/themes/newro_vcs.zsh-theme
sed -i 's/robbyrussell/newro_vcs/g' ~/.zshrc
sed -i 's/^# DISABLE_UNTRACKED_FILES_DIRTY/DISABLE_UNTRACKED_FILES_DIRTY/g' ~/.zshrc
sed -i 's/^# HIST_STAMPS/HIST_STAMPS/g' ~/.zshrc
sed -i '54s/^/export _Z_NO_RESOLVE_SYMLINKS="1"\n/' ~/.zshrc
perl -0777 -i.original -pe 's/plugins=\(\n.*git/plugins=\(\n\tgit svn tmux zsh-syntax-highlighting history-substring-search z zsh-autosuggestions/g' ~/.zshrc
echo "[ -f ~/develconfig/fzf.devel ] && source ~/develconfig/fzf.devel" >> ~/.fzf.zsh
echo "run on cmd[source $HOME/.zshrc]"
